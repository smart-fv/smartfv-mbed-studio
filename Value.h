#include "mbed.h"
#include "string"
#include "time.h"
using namespace std;

class Value {

    private:
    tm timestamp;     
    float produced;
    float consumed;

    public:
    Value() {
        produced = 0;
        consumed = 0;
    }
    Value(tm timestamp, float produced, float consumed) {
            this->timestamp = timestamp;            
            this->produced = produced;
            this->consumed = consumed;
    }
    tm getTimestamp() {
        return timestamp;
    }
    float getProduced() {
        return produced;
    }
    float getConsumed() {
        return consumed;
    }
    void setTimestamp(tm timestamp) {
        this->timestamp = timestamp;
    }
    void addEnergy(float produced, float consumed) {
        this->produced += produced;
        this->consumed += consumed;
    }
    void resetEnergy() {
        this->produced = 0;
        this->consumed = 0;
    }
};