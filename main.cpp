/* 
 *  SmartFV -- Programma per il controllo di impianti di prudizione FV , con monitoraggio continuo
 *            di potenza prodotta e autoconsumata e gestione di carico controllato.
*/

/*
 * N.B. Commentare sempre le printf di debug per ottimizzare i tempi di esecuzione
*/

#include "mbed.h"
#include "ESPAT.h"
#include "Master.h"
#include "FATFileSystem.h"
#include "SDBlockDevice.h"
#include "NTPClient.h"
#include "list"
#include "Value.h"

// DICHIARAZIONE FUNZIONI
#pragma region Dichiarazione Funzioni
void mountFS();
void initValueList();
void initTime();
void saveMode(int mode);
int whatMode();

void handleRequest(int linkId, string path);
void html();

void controllo();
void energia();
void timestamp(tm* cTimestamp);
void salvataggio();
void salvaUltimo();
void pwmVal();
void convertMan(float* manti, uint64_t mantissa);
void convertExp(float* exp, int exponent, int sign);
void convertEnergy();
#pragma endregion

// VARIABILI GLOBALI
#pragma region Variabili Globali
// Block Device per comunicare con la scheda SD
SDBlockDevice bd(MBED_CONF_SD_SPI_MOSI, MBED_CONF_SD_SPI_MISO, MBED_CONF_SD_SPI_CLK, MBED_CONF_SD_SPI_CS);
// File System della scheda SD  
FATFileSystem fs("fs"); 

// Lista in cui sono tenuti i rilevamenti (1 per giorno)
list<Value> vals;
// int measureCount = 0;
// Memorizzazione dati di produzione e consumo del singolo rilevamento 
float produzione, consumo;
// Giorno corrente
Value currValue;
// Valore pwm
int16_t val = 0;
// Consumo carico in kW
float carico = 2.2;
// Scarto fra produzione e consumo
int16_t scarto;

// Oggetto per la comunicazione con la scheda ESP8266 tramite i comandi AT
ESPAT* esp;
// Stringhe HTML
string htmlHead = "<html> <head> <meta charset=\"utf-8\"/> <title>SMART FV</title> </head>"
                "<body> <div style=\"color: red; text-align: center; font-weight: bolder; font-size: 125%; margin-bottom: 2%;\">SMART FV HOME</div>"
                "<div style=\"width: 100%; float: left;\"> <div style=\"width: 19%; margin: 0.3%; float: left;\"> <div> &nbsp</div> </div>"
                "<div style=\"width: 39%; margin: 0.3%; float: left;\"> <div style=\"font-weight:bold;\">Produzione</div> </div>"
                "<div style=\"width: 39%; margin: 0.3%; float: left;\"> <div style=\"font-weight:bold;\">Consumo</div> </div> </div>"; // Website code
string htmlEnd = "</body></html>";
string last, day, dayH, week, weekH, month, monthH;

// Array dove salvare i valori inviati dal contatore
uint16_t values[5] = {0, 0, 0, 0, 0};
// Istanza dell'oggetto per effettuare la comunicazione con Modbus
Master master(PA_11, PA_12, 9600, values, NULL);
#pragma endregion

// MAIN
int main() {
    // printf("Inizializzazione valori\r\n");  
    mountFS(); // Crea il file system sulla SD
    initValueList(); // Carica i valori salvati nella lista

    // Gestione del conflitto fra le modalità d'uso della scheda ESP8266 
    int mode = whatMode();
    if (mode == 1) {
        initTime();
        saveMode(0);
        __NVIC_SystemReset();
    }
    else {
        saveMode(1);
    }
    
    // Avvio del thread adibito a lettura valori, calcolo PWM, comunicazione valore 
    // printf("Avvio del thread di controllo\r\n");
    Thread thread1;     
    thread1.start(controllo);
         
    // Creazione del server usando la scheda ESP8266
    ESPAT esp8266(D8, D2, 115200);  
    esp = &esp8266;
    // printf("Reset dell'ESP\r\n");
    esp->resetEsp(); // Reset della scheda
    // printf("Inizializzazione AP\r\n");
    esp->initWifiAP("SFV", "12345678"); // Inizializzazione Access Point  
    // printf("Inizializzazione del SERVER\r\n");
    esp->initServer(handleRequest); // Inizializzazione del Request Handler    
    
    while (1) {
    }         
}  
     
// DEFINIZIONE FUNZIONI
#pragma region Inizializzazione
// Inizializza il file system
void mountFS() {
    int error = 0;
    // printf("Welcome to the filesystem example.\r\n" "Formatting a FAT, RAM-backed filesystem. ");
    /*
     * error = FATFileSystem::format(&bd);
     * if (error) {
     *   printf("Failure. %d\r\n", error);
     *   return;
     *  }
     *  printf("done.\r\n");
    */
    // printf("Mounting the filesystem on \"/fs\". ");
    error = fs.mount(&bd);
    if (error) {
        // printf("Failure. %d\r\n", error);
        return;
    }
    // printf("done.\r\n");    
}

// Carica in memoria i valori salvati su file
void initValueList() {
    // printf("Apertura del file\r\n");
    FILE* valsF = fopen("/fs/values.txt", "r");    
    if (valsF == NULL)  {
        // printf(" Failure\r\n");
        return;
    }
    printf(" done.\r\n");
    tm tp;
    float tP, tC;
    Value temp;
    while (fscanf(valsF, "%d %d %d %f %f\r\n", &tp.tm_mday, &tp.tm_mon, &tp.tm_year, &tP, &tC) != EOF) {
        temp = Value(tp, tP, tC);
        vals.push_back(temp);
    } 
    fclose(valsF); 
    valsF = fopen("/fs/lastValue.txt", "r");
    if (valsF == NULL) {
        return;
    } 
    if (fscanf(valsF, "%d %d %d %f %f\r\n", &tp.tm_mday, &tp.tm_mon, &tp.tm_year, &tP, &tC) != EOF) {
        currValue.setTimestamp(tp);
        currValue.addEnergy(tP, tC);
    }
    fclose(valsF);
     

}

// Recupera dal file la modalità di esecuzione della scheda ESP8266
int whatMode() {
    int mod;
    FILE* mode = fopen("/fs/mode.txt", "r");
    if (mode == NULL)  {
        // printf("FILE doesn't exists\r\n");
        return 1;
    }
    fscanf(mode, "%d\r\n", &mod);
    // printf("%d\r\n", mod);
    fclose(mode);
    return mod;
}

// Connette al server NTP, richiede il timestamp e lo scrive nel contatore
void initTime() {
    NetworkInterface* net = NetworkInterface::get_default_instance();      

    time_t timestamp;   
    if (!net) {
        // printf("Error! No network interface found!\r\n");
        return;
    }        
    // printf("Connecting to the network...\r\n");
    nsapi_error_t result = net->connect();
    if (result != 0) {
        // printf("Error! _net->connect() returned: %d\r\n", result);
        return;
    }  
    else {
        // printf("Connected Result %d\r\n", result);
    }
    NTPClient ntpclient(net);
    timestamp = ntpclient.get_timestamp();        
    if (timestamp < 0) {
        // printf("An error occurred when getting the time. Code: %ld\r\n", timestamp);
    } else {
        timestamp += 7200;
        // printf("Current time is %s\r\n", ctime(&timestamp));
    } 
    result = net->disconnect();
    net = NULL;        
    if (result != 0) {
        // printf("Error! _net->disconnect() returned: %d\r\n", result);
        return;
    }  
    else {
        // printf("Disconnected result: %d\r\n", result);
    }    

    struct tm* time;
    time = localtime(&timestamp);

    // Aggiustamento parametri x esadecimale
    time->tm_year -= 100;
    time->tm_mon += 1;
    uint16_t vals[4];

    // Conversione in formato esadecimale
    unsigned char tempMSB = (time->tm_sec / 10 * 16) + (time->tm_sec % 10);
    unsigned char tempLSB = (time->tm_min / 10 * 16) + (time->tm_min % 10);
    vals[0] = (tempMSB << 8) | tempLSB;
    tempMSB = (time->tm_hour / 10 * 16) + (time->tm_hour % 10);
    tempLSB = (time->tm_wday / 10 * 16) + (time->tm_wday % 10);
    vals[1] = (tempMSB << 8) | tempLSB;
    tempMSB = (time->tm_mday / 10 * 16) + (time->tm_mday % 10);
    tempLSB = (time->tm_mon / 10 * 16) + (time->tm_mon % 10);
    vals[2] = (tempMSB << 8) | tempLSB;
    tempMSB = (time->tm_year / 10 * 16) + (time->tm_year % 10);
    tempLSB = 0;
    vals[3] = (tempMSB << 8) | tempLSB;

    master.setVars(1, 16, 0x3C, 4, vals);
    master.cycle();     
}

// Salva la modalità di esecuzione della scheda ESP8266 per il prossimo avvio
void saveMode(int mode) {
    // printf("PRINT MODE %d\r\n", mode);
    FILE* mod;
    mod = fopen("/fs/mode.txt", "w");
    if (mod == NULL) {
        // printf("FILE doesn't exist\r\n\n");
        return;
    }
    fprintf(mod, "%d\r\n", mode);  
    fclose(mod);    
}
#pragma endregion

#pragma region Server
// Gestore delle richieste HTTP
void handleRequest(int linkId, string path) { // Bei HTTP Request     
    if (path == "/") { // Bei direkter IP
        html();
        esp->httpReply(linkId, "200 OK", htmlHead + last + dayH + day + weekH + week + monthH + month + htmlEnd); // Invia il sito
    } 
    else { // Invia errore 404 Not Found
        esp->httpReply(linkId, "404 Not Found", "404 Not found!");
    }
}

// Generazione del codice HTML di risposta
void html() {    
    char cTimetemp[30];
    string sTimetemp;
    float totProd, totCons;    
    float tProd = 0;
    float tCons = 0;       
    list<Value>::iterator it;
    Value temp;    
    tm tTime;   
    
    dayH = "<div style=\"border-style: solid; margin: 0.5%; width: 99%; float: left;\"><div style=\"width: 19%; margin: 0.3%; float: left;\">"
            "<div style=\"font-weight:bold;\">Oggi</div></div><div style=\"width: 39%; margin: 0.3%; float: left;\">"
            "<div><b>Totale:</b> " + to_string(currValue.getProduced()) + " kWh</div></div> <div style=\"width: 39%; margin: 0.3%; float: left;\">"
            "<div><b>Totale:</b> " + to_string(currValue.getConsumed()) + " kWh</div></div></div>";

    // Energia ultimi 7 giorni 
    int k = 0; 
    week = "";
    totProd = totCons = 0;
    tProd = currValue.getProduced();
    tCons = currValue.getConsumed(); 
    tTime = currValue.getTimestamp();       
    sprintf(cTimetemp, "%02d/%02d/%02d", tTime.tm_mday, tTime.tm_mon, tTime.tm_year);
    sTimetemp.assign(cTimetemp);
    week += "<div><div style=\"width: 19%; margin: 0.3%; float: left;\"><div>" + sTimetemp + "</div>"
            "</div><div style=\"width: 39%; margin: 0.3%; float: left;\"><div>" + to_string(tProd) + " kWh</div>"
            "</div><div style=\"width: 39%; margin: 0.3%; float: left;\"><div>" + to_string(tCons) + " kWh</div>"
            "</div></div>";
    totProd += tProd;
    totCons += tCons;   
    for (it = vals.begin(); it != vals.end(); it++) {
        if (k == 6) {
            break;
        }
        temp = *it;
        tTime = temp.getTimestamp();
        tProd = temp.getProduced();
        tCons = temp.getConsumed();        
        sprintf(cTimetemp, "%02d/%02d/%02d", tTime.tm_mday, tTime.tm_mon, tTime.tm_year);
        sTimetemp.assign(cTimetemp);
        week += "<div><div style=\"width: 19%; margin: 0.3%; float: left;\"><div>" + sTimetemp + "</div>"
                "</div><div style=\"width: 39%; margin: 0.3%; float: left;\"><div>" + to_string(tProd) + " kWh</div>"
                "</div><div style=\"width: 39%; margin: 0.3%; float: left;\"><div>" + to_string(tCons) + " kWh</div>"
                "</div></div>";
        totProd += tProd;
        totCons += tCons; 
        k++;            
    }        
    week += "</div>";
    weekH = "<div style=\"border-style: solid; margin: 0.5%; width: 99%; float: left;\"> <div style=\"width: 19%; margin: 0.3%; float: left;\">"
            "<div style=\"font-weight:bold;\">Ultimi 7 Giorni</div> </div> <div style=\"width: 39%; margin: 0.3%; float: left;\">"
            "<div><b>Totale:</b> " + to_string(totProd) + " kWh</div></div> <div style=\"width: 39%; margin: 0.3%; float: left;\">"
            "<div><b>Totale:</b> " + to_string(totCons) + " kWh</div></div>";         
    
    // Energia ultimi 7 giorni
    month = "";
    totProd = totCons = 0;
    tProd = currValue.getProduced();
    tCons = currValue.getConsumed(); 
    tTime = currValue.getTimestamp();       
    sprintf(cTimetemp, "%02d/%02d/%02d", tTime.tm_mday, tTime.tm_mon, tTime.tm_year);
    sTimetemp.assign(cTimetemp);
    month += "<div><div style=\"width: 19%; margin: 0.3%; float: left;\"><div>" + sTimetemp + "</div>"
            "</div><div style=\"width: 39%; margin: 0.3%; float: left;\"><div>" + to_string(tProd) + " kWh</div>"
            "</div><div style=\"width: 39%; margin: 0.3%; float: left;\"><div>" + to_string(tCons) + " kWh</div>"
            "</div></div>";
    totProd += tProd;
    totCons += tCons;
    k = 0;   
    for (it = vals.begin(); it != vals.end(); it++) {
        if (k == 29) {
            break;
        }
        temp = *it;
        tTime = temp.getTimestamp();
        tProd = temp.getProduced();
        tCons = temp.getConsumed();        
        sprintf(cTimetemp, "%02d/%02d/%02d", tTime.tm_mday, tTime.tm_mon, tTime.tm_year);
        sTimetemp.assign(cTimetemp);
        month += "<div><div style=\"width: 19%; margin: 0.3%; float: left;\"><div>" + sTimetemp + "</div>"
                "</div><div style=\"width: 39%; margin: 0.3%; float: left;\"><div>" + to_string(tProd) + " kWh</div>"
                "</div><div style=\"width: 39%; margin: 0.3%; float: left;\"><div>" + to_string(tCons) + " kWh</div>"
                "</div></div>";
        totProd += tProd;
        totCons += tCons; 
        k++;            
    }        
    month += "</div>";
    monthH = "<div style=\"border-style: solid; margin: 0.5%; width: 99%; float: left;\"> <div style=\"width: 19%; margin: 0.3%; float: left;\">"
            "<div style=\"font-weight:bold;\">Ultimi 30 Giorni</div> </div> <div style=\"width: 39%; margin: 0.3%; float: left;\">"
            "<div><b>Totale:</b> " + to_string(totProd) + " kWh</div></div> <div style=\"width: 39%; margin: 0.3%; float: left;\">"
            "<div><b>Totale:</b> " + to_string(totCons) + " kWh</div></div>"; 
      
}
#pragma endregion

#pragma region Controllo
// Accorpa di tutte le funzionalità di controllo
void controllo() {  
    Timer timer; 
    tm time = currValue.getTimestamp();
    int day = time.tm_mday;
    int micros;     
    timer.start();       
    while (true) {        
        energia();  
        convertEnergy();
        pwmVal();        
        timestamp(&time);        
        if (time.tm_mday != day) {
            if (day != 0) {
                salvataggio();
            }            
            currValue.resetEnergy();
            currValue.setTimestamp(time);
            day = time.tm_mday;
        }
        currValue.addEnergy(produzione / 360, consumo / 360); 
        salvaUltimo();
        micros = timer.elapsed_time().count();
        if (micros < 10000000) {
            wait_us(10000000 - micros);
        }                   
        // printf("Fine ciclo\r\n");        
        timer.reset();
    }   
}

// Salva i valori su file
void salvataggio() {
    // printf("Inizio del salvataggio\r\n");   
    Value temp;
    tm tp;
    // listAvailable.lock();
    // Inserisco nuovo elemento nella lista
    // printf("Push all'inizio della lista\r\n");
    vals.push_front(currValue);
    // Elimina elemento quando viene passato il massimo dei valori
    if (vals.size() > 150) {
        // printf("Eliminazione dalla coda della lista\r\n");
        vals.pop_back();
    }
    // listAvailable.unlock();

    // printf("Apertura del file\r\n");
    FILE* valsF = fopen("/fs/values.txt", "w");   
    if (valsF == NULL)  {
        // printf(" Failure\r\n");
        return;
    }
    // printf(" done.\r\n");

    list<Value>::iterator it;  
    // printf("Inizio loop per il salvataggio su file\r\n"); 
    for ( it = vals.begin(); it != vals.end(); it++) {
        temp = *it;
        tp = temp.getTimestamp();      
        // printf("Scrittura su file...\r\n");  
        fprintf(valsF, "%d %d %d %f %f\r\n", tp.tm_mday, tp.tm_mon, tp.tm_year, temp.getProduced(), temp.getConsumed());
    }
    // printf("Chiusura del file\r\n");
    fclose(valsF);
}

void salvaUltimo() {
    FILE* valsF = fopen("/fs/lastValue.txt", "w");
    if (valsF == NULL)  {
        // printf(" Failure\r\n");
        return;
    }
    tm tp;
    tp = currValue.getTimestamp();
    fprintf(valsF, "%d %d %d %f %f\r\n", tp.tm_mday, tp.tm_mon, tp.tm_year, currValue.getProduced(), currValue.getConsumed());
    fclose(valsF);
}

// Richiede il timestamp al contatore e lo converte nel formato per la struct tm
void timestamp(tm* cTimestamp) {    
    master.setVars(1, 3, 60, 4);
    master.cycle();

    // Conversione
    // int temp = values[0] >> 8;    
    // cTimestamp->tm_sec = (temp /16) * 10 + (temp % 16);
    // temp = values[0] &0xFF;
    // cTimestamp->tm_min = (temp /16) * 10 + (temp % 16);
    // temp = values[1] >> 8;
    // cTimestamp->tm_hour = (temp /16) * 10 + (temp % 16);
    int temp = values[2] >> 8;
    cTimestamp->tm_mday = (temp /16) * 10 + (temp % 16);
    temp = values[2] &0xFF;
    cTimestamp->tm_mon = (temp /16) * 10 + (temp % 16);
    temp = values[3] >> 8;
    cTimestamp->tm_year = (temp /16) * 10 + (temp % 16);
    // printf("%d:%d:%d  %d/%d/%d\r\n", cTimestamp.tm_hour, cTimestamp.tm_min, cTimestamp.tm_sec, cTimestamp.tm_mday, cTimestamp.tm_mon, cTimestamp.tm_year);         
}

// Richiede i valori di energia prodotta e consumata e li converte in virgola mobile
void energia() {    
    master.setVars(1, 3, 30, 4);
    master.cycle();  
    convertEnergy(); 
}

/* 
* Converte i valori di energia prodotta e consumata da interi a virgola mobile e li moltiplica per il tempo in ore
* (varia in base alla frequenza dei rilevamenti) per trasformare il valore da kW a kWh
*/
void convertEnergy() {
    // Produzione  
    uint32_t intPower = (values[0] << 16) | values[1]; 
    int sign = intPower >> 31;
    int exponent = ((intPower & 0x7FFFFFFF) >> 23) - 127;
    float floatExp = 1.0;
    convertExp(&floatExp, exponent, sign);
    int mantissa = intPower & 0x7FFFFF;    
    float floatManti = 0;
    convertMan(&floatManti, (uint64_t)mantissa);
    // printf("Mantissa: %f\r\n", floatManti);
    produzione = floatManti * floatExp;

    // Consumo
    intPower = (values[2] << 16) | values[3];
    sign = intPower >> 31;
    exponent = ((intPower & 0x7FFFFFFF) >> 23) - 127;
    floatExp = 1.0;
    convertExp(&floatExp, exponent, sign);
    mantissa = intPower & 0x7FFFFF;    
    floatManti = 0;
    convertMan(&floatManti, (uint64_t)mantissa);
    // printf("Mantissa: %f\r\n", floatManti);
    consumo = floatManti * floatExp;
}

// Ricostruisce la mantissa del float_32 a partire dall'intero di partenza
void convertMan(float* manti, uint64_t mantissa) {
    float div = 1.0;
    for (int i = 0; i < 23; i++) {
        div *= 2.0;
        *manti += (((mantissa << i) & 0x400000) >> 22) / div;
    }
    *manti += 1.0;
}

// Ricostruisce l'esponente del float_32 a partire dall'intero di partenza
void convertExp(float* exp, int exponent, int sign) {
    if (exponent > 0){
        for (int i = 0; i < exponent; i++) {
            *exp *= 2.0;
        }
    } 
    else {
        for (int i = exponent; i < 0; i++) {
            *exp /= 2.0;
        }
    }   
    if (sign == 1) {
        *exp *= -1.0;        
    }
}

// Calcola il valore pwm e lo comunica alla scheda che si occupa dell'uscita
void pwmVal() {
    printf("Valore produzione: %f\r\n", produzione);
    printf("Valore consumo: %f\r\n", consumo);
    scarto = (produzione - consumo) / carico * 1000;    
    val += scarto;
    // Valore massimo = 100
    if (val > 1000) {
        val = 1000;
    }
    // Valore minimo = 0
    else if (val < 0) {
        val = 0;
    }     
    printf("Valore PWM: %d\r\n", val);

    master.setVars(2, 6, 0, 500);
    master.cycle();
}
#pragma endregion